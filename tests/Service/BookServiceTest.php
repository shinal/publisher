<?php

namespace App\Tests\Service;

use App\Entity\Book;
use App\Exception\BookCategoryNotFoundException;
use App\Model\BookListItem;
use App\Model\BookListResponse;
use App\Repository\BookCategoryRepository;
use App\Repository\BookRepository;
use App\Repository\ReviewRepository;
use App\Service\BookService;
use App\Tests\AbstractTestCase;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\MockObject\Exception;

class BookServiceTest extends AbstractTestCase
{
    /**
     * @throws Exception
     */
    public function testGetBookByCategoryNotFound(): void
    {
        $reviewRepository = $this->createMock(ReviewRepository::class);
        $bookRepository = $this->createMock(BookRepository::class);
        $bookCategoryRepository = $this->createMock(BookCategoryRepository::class);
        $bookCategoryRepository->expects($this->once())
            ->method('existsById')
            ->with(130)
            ->willReturn(false);

        $this->expectException(BookCategoryNotFoundException::class);

        (new BookService($bookRepository, $bookCategoryRepository, $reviewRepository))->getBookByCategory(130);
    }

    /**
     * @throws Exception
     */
    public function testGetBookByCategory(): void
    {
        $reviewRepository = $this->createMock(ReviewRepository::class);
        $bookRepository = $this->createMock(BookRepository::class);
        $bookRepository->expects($this->once())
            ->method('findBookByCategoryId')
            ->with('130')
            ->willReturn([$this->createBookEntity()]);

        $bookCategoryRepository = $this->createMock(BookCategoryRepository::class);
        $bookCategoryRepository->expects($this->once())
            ->method('existsById')
            ->with(130)
            ->willReturn(true);

        $bookService = new BookService($bookRepository, $bookCategoryRepository, $reviewRepository);

        $expected = new BookListResponse([$this->createBookItemModel()]);

        $this->assertEquals($expected, $bookService->getBookByCategory(130));
    }

    public function createBookEntity(): Book
    {
        $book = (new Book())
            ->setTitle('testBook')
            ->setSlug('test-book')
            ->setMeap(false)
            ->setIsbn('123456')
            ->setDescription('test description')
            ->setAuthors(['Testers'])
            ->setCategorias(new ArrayCollection())
            ->setImage('http://localhost/test.png')
            ->setPublicationDate(new DateTimeImmutable('2020-10-10'));

        $this->setEntityId($book, 123);

        return $book;
    }

    public function createBookItemModel(): BookListItem
    {
        return (new BookListItem())
            ->setId(123)
            ->setTitle('testBook')
            ->setSlug('test-book')
            ->setMeap(false)
            ->setAuthors(['Testers'])
            ->setImage('http://localhost/test.png')
            ->setPublicationDate(1602288000);
    }
}
