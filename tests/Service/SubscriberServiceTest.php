<?php

namespace App\Tests\Service;

use App\Entity\Subscriber;
use App\Exception\SubscriberAlreadyExistException;
use App\Model\SubscriberRequest;
use App\Repository\SubscriberRepository;
use App\Service\SubscriberService;
use App\Tests\AbstractTestCase;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class SubscriberServiceTest extends AbstractTestCase
{
    private SubscriberRepository $subscriberRepository;
    private EntityManagerInterface $entityManager;
    const EMAIL = 'test@test.com';

    protected function setUp(): void
    {
        parent::setUp();
        $this->subscriberRepository = $this->createMock(SubscriberRepository::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
    }

    public function testSubscribeAlreadyExist(): void
    {
        $this->expectException(SubscriberAlreadyExistException::class);

        $this->subscriberRepository->expects($this->once())
            ->method('existsByEmail')
            ->with(self::EMAIL)
            ->willReturn(true);

        $request = new SubscriberRequest();
        $request->setEmail(self::EMAIL);


        (new SubscriberService($this->subscriberRepository, $this->entityManager))->subscribe($request);

    }

    public function testSubscribe(): void
    {
        $this->subscriberRepository->expects($this->once())
            ->method('existsByEmail')
            ->with(self::EMAIL)
            ->willReturn(false);

        $request = new SubscriberRequest();
        $request->setEmail(self::EMAIL);

        $expectedSubscriber = new Subscriber();
        $expectedSubscriber->setEmail(self::EMAIL);

        $this->entityManager->expects($this->once())
            ->method('persist')
            ->with($expectedSubscriber);

        $this->entityManager->expects($this->once())
            ->method('flush');


        (new SubscriberService($this->subscriberRepository, $this->entityManager))->subscribe($request);

    }
}
