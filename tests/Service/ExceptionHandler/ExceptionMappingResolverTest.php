<?php

namespace App\Tests\Service\ExceptionHandler;

use App\Service\ExceptionHandler\ExceptionMappingResolver;
use App\Tests\AbstractTestCase;
use InvalidArgumentException;

class ExceptionMappingResolverTest extends AbstractTestCase
{
    public function testResolveThrowsExceptionOnEmptyCode(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new ExceptionMappingResolver(['someClass' => ['hidden' => true]]);
    }

    public function testResolveReturnsNullWhenNotFound(): void
    {
        $resolver = new ExceptionMappingResolver([]);
        $this->assertNull($resolver->resolve(InvalidArgumentException::class));
    }

    public function testResolveClassItself(): void
    {
        $resolver = new ExceptionMappingResolver([InvalidArgumentException::class => ['code' => 400, 'hidden' => false]]);
        $mapping = $resolver->resolve(InvalidArgumentException::class);

        $this->assertEquals(400, $mapping->getCode());
    }

    public function testResolvesSubClass()
    {
        $resolver = new ExceptionMappingResolver([\LogicException::class => ['code' => 500, 'hidden' => false]]);
        $mapping = $resolver->resolve(InvalidArgumentException::class);

        $this->assertEquals(500, $mapping->getCode());
    }

    public function testResolvesHidden()
    {
        $resolver = new ExceptionMappingResolver([\LogicException::class => ['code' => 500, 'hidden' => false]]);
        $mapping = $resolver->resolve(\LogicException::class);

        $this->assertFalse($mapping->isHidden());
    }

    public function testResolvesLoggable()
    {
        $resolver = new ExceptionMappingResolver([\LogicException::class => ['code' => 500, 'hidden' => false, 'loggable' => true]]);
        $mapping = $resolver->resolve(\LogicException::class);

        $this->assertTrue($mapping->isLoggable());
    }
}
