<?php

namespace App\Tests\Controller;

use App\Controller\SubscribeController;
use App\Tests\AbstractControllerTest;
use App\Tests\AbstractTestCase;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class SubscribeControllerTest extends AbstractControllerTest
{

    public function testSubscribe(): void
    {
        $content = json_encode(['email' => 'test@test.com', 'agree' => true]);
        $this->client->request('POST', '/api/v1/subscribe', [], [], [], $content);

        $this->assertResponseIsSuccessful();
    }

    public function testSubscribeNotAgree(): void
    {
        $content = json_encode(['email' => 'test@test.com']);
        $this->client->request('POST', '/api/v1/subscribe', [], [], [], $content);

        $responseContent = json_decode($this->client->getResponse()->getContent());

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
        $this->assertJsonDocumentMatches($responseContent, [
            '$.message' => 'validation failed',
            '$.details.violation' => self::countOf(1),
            '$.details.violation[0].field' => 'agree',
        ]);
    }
}
