<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\NotBlank;

class SubscriberRequest
{
    #[Email]
    #[NotBlank]
    private string $email;

    #[IsTrue]
    #[NotBlank]
    private bool $agree;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): SubscriberRequest
    {
        $this->email = $email;

        return $this;
    }

    public function isAgree(): bool
    {
        return $this->agree;
    }

    public function setAgree(bool $agree): SubscriberRequest
    {
        $this->agree = $agree;

        return $this;
    }
}
