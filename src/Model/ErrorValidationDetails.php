<?php

namespace App\Model;

class ErrorValidationDetails
{
    private array $violation = [];

    public function addViolation(string $field, string $message)
    {
        $this->violation[] = new ErrorValidationDetailsItem($field, $message);
    }

    public function getViolation(): array
    {
        return $this->violation;
    }

}
