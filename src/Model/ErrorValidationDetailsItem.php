<?php

namespace App\Model;

class ErrorValidationDetailsItem
{

    /**
     * @param string $field
     * @param string $message
     */
    public function __construct(private string $field, private string $message)
    {
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getField(): string
    {
        return $this->field;
    }
}
