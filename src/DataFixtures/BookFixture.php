<?php

namespace App\DataFixtures;

use App\Entity\Book;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class BookFixture extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $androidCategory = $this->getReference(BookCategoryFixtures::ANDROID_CATEGORY);
        $devicesCategory = $this->getReference(BookCategoryFixtures::DEVICES_CATEGORY);

        $book = (new Book())
            ->setTitle('Rx Java for Android Developers')
            ->setPublicationDate(new DateTimeImmutable('2019-04-01'))
            ->setMeap(false)
            ->setIsbn('12345')
            ->setDescription('test description')
            ->setAuthors(['Ihor Shynal'])
            ->setSlug('rx-java-for-android-developers')
            ->setCategorias(new ArrayCollection([$androidCategory, $devicesCategory]))
            ->setImage('https://images.manning.com/264/352/resize/book/f/e7a0e97-9ca2-423c-a0b9-8c5f000158bf/Hao-HI-MEAP.png');

        $manager->persist($book);
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            BookCategoryFixtures::class,
        ];
    }
}
