<?php

namespace App\Controller;

use App\Service\BookCategoryService;
use App\Service\ReviewService;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Model\ReviewPage;

class ReviewController extends AbstractController
{
    public function __construct(private ReviewService $reviewService)
    {
    }

    /**
     * @OA\Parameter(name="page", in="query", description="Page number", @OA\Schema(type="integer"))
     * @OpenApi\Response(
     *     response=200,
     *     description="Review Page"
     * @Model(type=ReviewPage::class)
     * )
     */
    #[Route(path: '/api/v1/book/{id}/reviews', methods: 'GET')]
    public function reviews(Request $request, int $id): Response
    {
        return $this->json($this->reviewService->getReviewPageByBookId($id, $request->query->get('page', 1)));
    }
}
