<?php

namespace App\Controller;

use App\Service\BookCategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Model\BookCategoryListResponse;

class BookCategoryController extends AbstractController
{
    public function __construct(private readonly BookCategoryService $bookCategoryService)
    {
    }

    /**
     * @OpenApi\Response(
     *     response=200,
     *     description="Return books category"
     * @Model(type=BookCategoryListResponse::class)
     * )
     */
    #[Route('/api/v1/book/categories')]
    public function categories(): Response
    {
        return $this->json($this->bookCategoryService->getCategories());
    }
}
