<?php

namespace App\Controller;

use App\Service\BookService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Model\ErrorResponse;
use App\Model\BookListResponse;
use App\Model\BookDetails;

class BookController extends AbstractController
{
    public function __construct(private BookService $bookService)
    {
    }

    /**
     * @OpenApi\Response(
     *     response=200,
     *     description="Return books by category"
     * @Model(type=BookListResponse::class)
     * )
     * @OpenApi\Response(
     *     response=404
     *     description="book category not found"
     * @Model(type=ErrorResponse::class)
     * )
     */
    #[Route('/api/v1/book/category/{id}/books')]
    public function booksByCategory(int $id): Response
    {
        return $this->json($this->bookService->getBookByCategory($id));
    }

    /**
     * @OpenApi\Response(
     *     response=200,
     *     description="Return book by id"
     * @Model(type=BookDetails::class)
     * )
     * @OpenApi\Response(
     *     response=404
     *     description="book category not found"
     * @Model(type=ErrorResponse::class)
     * )
     */
    #[Route('/api/v1/book/book/{id}')]
    public function booksById(int $id): Response
    {
        return $this->json($this->bookService->getBookById($id));
    }
}
